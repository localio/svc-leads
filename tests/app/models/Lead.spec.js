const { expect } = require('chai');
const mongoose = require('mongoose');
const LeadModel = require('../../../app/models/Lead');
const { modelFixture } = require('../../fixtures/lead');
require('../../../app/lib/mongo');

// Set up default Promise library to be used with mongoose
mongoose.Promise = global.Promise;

// Get list of available languages
// const langs = config.INTL_AVAILABLE.split(/,/g);

describe('Lead model', () => {
  // Prepare
  before((done) => { // Before all tests we empty the database
    LeadModel.remove({}, () => {
      done();
    });
  });
  after((done) => { // After all tests we empty the database
    LeadModel.remove({}, () => {
      done();
    });
  });

  it('should complain if type is wrong', done => {
    const fixture = modelFixture();
    fixture.type = '?!no_a_type_at_all!?';
    const Item = new LeadModel(fixture);
    Item.validate(err => {
      expect(err.errors.type).to.exist;
      done();
    });
  });
  it('should be invalid if not all required data here in place', (done) => {
    const Item = new LeadModel();
    Item.validate((err) => {
      expect(err.errors.name).to.exist;
      expect(err.errors.type).to.exist;
      done();
    });
  });
  it('should be invalid if name is not specified in contact records', done => {
    const fixture = modelFixture();
    delete fixture.contacts[0].name;
    const Item = new LeadModel(fixture);
    Item.validate(err => {
      expect(err.errors['contacts.0.name']).to.exist;
      done();
    });
  });
  it('should create object with no complains', (done) => {
    const fixture = modelFixture();
    const Item = new LeadModel(fixture);
    Item.validate((err) => {
      const json = Item.toJSON();
      expect(err).to.be.null;
      expect(json.name).to.be.equal(fixture.name);
      expect(json.type).to.be.equal(fixture.type);
      expect(`${json.address}`).to.be.equal(fixture.address);
      expect(json.source).to.be.equal(fixture.source);
      expect(json.status).to.be.equal(fixture.status);
      // Compare phones
      const phones = json.phones.map(obj => ({ phone: obj.phone })).sort();
      expect(fixture.phones.sort()).to.be.deep.equal(phones);
      // Compare comments
      const comments = json.comments.map(obj => ({
        comment: obj.comment,
        date: obj.date
      })).sort();
      expect(fixture.comments.sort()).to.be.deep.equal(comments);
      // compare emails
      const emails = json.emails.map(obj => ({
        email: obj.email
      })).sort();
      expect(fixture.emails.sort()).to.be.deep.equal(emails);
      // compare cuisines
      const cuisines = json.cuisines.map(obj => ({ cuisine: obj.cuisine })).sort();
      expect(fixture.cuisines.sort()).to.be.deep.equal(cuisines);
      // compare contacts
      // assume it will have the same order
      fixture.contacts.forEach((contact, index) => {
        const contactToCompare = json.contacts[index];
        expect(contact.position).to.be.equal(contactToCompare.position);
        expect(contact.comment).to.be.equal(contactToCompare.comment);
        expect(contact.name).to.be.equal(contactToCompare.name);
        // Phones
        const contactPhones = contactToCompare.phones.map(obj => ({ phone: obj.phone })).sort();
        expect(contact.phones.sort()).to.be.deep.equal(contactPhones);
        // emails
        const contactEmails = contactToCompare.emails.map(obj => ({ email: obj.email })).sort();
        expect(contact.emails.sort()).to.be.deep.equal(contactEmails);
      });
      done();
    });
  });
  it('should create created and updated at values', (done) => {
    const fixture = modelFixture();
    const Item = new LeadModel(fixture);
    Item.save((err, json) => {
      expect(err).is.null;
      expect(json.createdAt).to.exist.and.to.be.instanceOf(Date);
      expect(json.updatedAt).to.exist.and.to.be.instanceOf(Date);
      expect(json.updatedAt.toString()).to.be.equal(json.createdAt.toString());
      done();
    });
  });
});
