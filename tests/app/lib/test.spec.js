/* eslint-disable no-undef */
const { expect } = require('chai');
const test = require('../../../app/lib/test');

describe('Test', () => {
  it('should return true if value more than zero', () => {
    expect(test(10)).to.be.equal(true);
  });
  it('should return fale if value less than zero', () => {
    expect(test(-10)).to.be.equal(false);
  });
});
