const faker = require('faker');
const mongoId = require('mongo-testid');
const { leads: { types: leadTypes } } = require('@dostolu/constants');
const { leads: { status: leadStatus } } = require('@dostolu/constants');

/**
 * Make contact fixture
 */
const contactFixture = () => ({
  position: faker.name.jobTitle(),
  phones: [
    { phone: faker.phone.phoneNumber() },
    { phone: faker.phone.phoneNumber() },
    { phone: faker.phone.phoneNumber() }
  ],
  emails: [
    { email: faker.internet.email() },
    { email: faker.internet.email() }
  ],
  name: faker.name.findName(),
  comment: faker.lorem.sentence()
});

/**
 * Make model fixture
 */
const modelFixture = () => ({
  name: faker.company.companyName(),
  // slug: faker.helpers.slugify(faker.commerce.productName()),
  type: faker.random.arrayElement([
    leadTypes.LEAD_TYPE_COLD,
    leadTypes.LEAD_TYPE_WARM,
    leadTypes.LEAD_TYPE_QUALIFIED
  ]),
  status: faker.random.arrayElement([
    leadStatus.LEAD_STATUS_NEW,
    leadStatus.LEAD_STATUS_OPEN,
    leadStatus.LEAD_STATUS_CLOSED
  ]),
  comments: [{
    comment: faker.lorem.sentence(),
    date: new Date()
  }, {
    comment: faker.lorem.sentence(),
    date: new Date()
  }],
  emails: [
    { email: faker.internet.email() },
    { email: faker.internet.email() },
    { email: faker.internet.email() }
  ],
  contacts: [
    contactFixture(),
    contactFixture()
  ],
  address: mongoId('address_id'),
  phones: [
    { phone: faker.phone.phoneNumber() },
    { phone: faker.phone.phoneNumber() }
  ],
  cuisines: [
    { cuisine: faker.commerce.productName() },
    { cuisine: faker.commerce.productName() },
    { cuisine: faker.commerce.productName() }
  ],
  source: faker.lorem.word()
});

module.exports.contactFixture = contactFixture;
module.exports.modelFixture = modelFixture;
