process.env.NODE_ENV = 'testing';
process.env.PORT = 9999;

const chai = require('chai');
const { expect } = require('chai');
const chaiHttp = require('chai-http');
const mongoId = require('mongo-testid');
const server = require('../../app/server/api');
const Lead = require('../../app/models/Lead');
const leadFixture = require('../fixtures/lead');
const { leads: { status: leadStatus } } = require('@dostolu/constants');

chai.use(chaiHttp);
let id = null;

describe('Leads', () => {
  // Prepare
  before((done) => { // Before all tests we empty the database
    Lead.remove({}, () => {
      done();
    });
  });
  after((done) => { // After all tests we empty the database
    Lead.remove({}, () => {
      done();
    });
  });

  describe('READ', () => {
    it('should get an empty list', (done) => {
      chai.request(server)
        .get('/leads')
        .then((res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res).to.have.header('Content-Type', 'application/json; charset=utf-8');
          const json = res.body;
          expect(json.status).to.be.equal('success');
          expect(json.data).to.have.lengthOf(0);
          expect(json.meta.total).to.be.equal(0);
          expect(json.meta.offset).to.be.equal(0);
          expect(json.meta.limit).to.be.equal(10);
          done();
        });
    });
    it('should return 404 if lead does not exists', (done) => {
      chai.request(server)
        .get(`/leads/${mongoId('leads')}`)
        .catch((err) => {
          const res = err.response;
          expect(res).to.have.status(404);
          expect(res).to.be.json;
          expect(res).to.have.header('Content-Type', 'application/json; charset=utf-8');
          const json = res.body;
          expect(json.status).to.be.equal('fail');
          expect(json.meta).to.not.exist;
          done();
        });
    });
  });

  describe('CREATE', () => {
    it('Should return error if input data is wrong', (done) => {
      chai.request(server)
        .post('/leads')
        .send({})
        .catch(err => {
          const res = err.response;
          expect(res).to.have.status(405);
          expect(res).to.be.json;
          expect(res).to.have.header('Content-Type', 'application/json; charset=utf-8');
          const json = res.body;
          expect(json.status).to.be.equal('fail');
          expect(json.meta).to.not.exist;
          done();
        });
    });
    it('Should create record if everything is all right', (done) => {
      const fixture = leadFixture.modelFixture();
      chai.request(server)
        .post('/leads')
        .send(fixture)
        .then((res) => {
          expect(res).to.have.status(201);
          expect(res).to.be.json;
          expect(res).to.have.header('Content-Type', 'application/json; charset=utf-8');
          const json = res.body.data;
          // Check all properties
          expect(json.name).to.be.equal(fixture.name);
          expect(json.type).to.be.equal(fixture.type);
          expect(`${json.address}`).to.be.equal(fixture.address);
          expect(json.source).to.be.equal(fixture.source);
          expect(json.status).to.be.equal(fixture.status);
          // Compare phones
          const phones = json.phones.map(obj => ({ phone: obj.phone })).sort();
          expect(fixture.phones.sort()).to.be.deep.equal(phones);
          // Compare comments
          const comments = json.comments.map(obj => ({
            comment: obj.comment,
            date: new Date(obj.date)
          })).sort();
          expect(fixture.comments.sort()).to.be.deep.equal(comments);
          // compare emails
          const emails = json.emails.map(obj => ({
            email: obj.email
          })).sort();
          expect(fixture.emails.sort()).to.be.deep.equal(emails);
          // compare cuisines
          const cuisines = json.cuisines.map(obj => ({ cuisine: obj.cuisine })).sort();
          expect(fixture.cuisines.sort()).to.be.deep.equal(cuisines);
          // compare contacts
          // assume it will have the same order
          fixture.contacts.forEach((contact, index) => {
            const contactToCompare = json.contacts[index];
            expect(contact.position).to.be.equal(contactToCompare.position);
            expect(contact.comment).to.be.equal(contactToCompare.comment);
            expect(contact.name).to.be.equal(contactToCompare.name);
            // Phones
            const contactPhones = contactToCompare.phones.map(obj => ({ phone: obj.phone })).sort();
            expect(contact.phones.sort()).to.be.deep.equal(contactPhones);
            // emails
            const contactEmails = contactToCompare.emails.map(obj => ({ email: obj.email })).sort();
            expect(contact.emails.sort()).to.be.deep.equal(contactEmails);
          });
          id = json._id;
          done();
        });
    });
    it('Should be able to get recent created record', (done) => {
      chai.request(server)
        .get(`/leads/${id}`)
        .then((res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res).to.have.header('Content-Type', 'application/json; charset=utf-8');
          expect(res.body.data._id).to.be.equal(id);
          done();
        });
    });
    it('should get the leads list', (done) => {
      chai.request(server)
        .get('/leads')
        .then((res) => {
          expect(res.status).to.be.equal(200);
          expect(res.body.status).to.be.equal('success');
          expect(res.body.meta.total).to.be.equal(1);
          expect(res.body.meta.offset).to.be.equal(0);
          expect(res.body.data[0]._id).to.be.equal(id);
          done();
        });
    });
  });

  describe('UPDATE', () => {
    it('should return 400 when trying to find with wrong id format', (done) => {
      chai.request(server)
        .put('/leads/neexistuje')
        .send({})
        .catch((err) => {
          expect(err.status).to.be.equal(400);
          expect(err.response.body.status).to.be.equal('fail');
          done();
        });
    });
    it('should update lead by id', (done) => {
      chai.request(server)
        .put(`/leads/${id}`)
        .send({
          status: leadStatus.LEAD_STATUS_OPEN
        })
        .then((res) => {
          expect(res.status).to.be.equal(201);
          done();
        });
    });
    it('should match new data now', (done) => {
      chai.request(server)
        .get(`/leads/${id}`)
        .then((res) => {
          expect(res.status).to.be.equal(200);
          expect(res.body.data.status)
            .to.be.deep.equal(leadStatus.LEAD_STATUS_OPEN);
          done();
        });
    });
  });

  describe('DELETE', () => {
    it('should return 404if trying to remove non existed lead', (done) => {
      chai.request(server)
        .delete(`/leads/${mongoId('neexistuje')}`)
        .catch((err) => {
          expect(err.status).to.be.equal(404);
          expect(err.response.body.status).to.be.equal('fail');
          done();
        });
    });
    it('should return 405 if trying to remove with wrong id', (done) => {
      chai.request(server)
        .delete('/leads/neexistuje')
        .catch((err) => {
          expect(err.status).to.be.equal(405);
          expect(err.response.body.status).to.be.equal('fail');
          done();
        });
    });
    it('should remove record', (done) => {
      chai.request(server)
        .delete(`/leads/${id}`)
        .then((res) => {
          expect(res.status).to.be.equal(204);
          done();
        });
    });
    it('it should not be able to find removed item', (done) => {
      chai.request(server)
        .delete(`/leads/${id}`)
        .catch((err) => {
          const res = err.response;
          expect(res).to.have.status(404);
          expect(res).to.be.json;
          expect(res).to.have.header('Content-Type', 'application/json; charset=utf-8');
          const json = res.body;
          expect(json.status).to.be.equal('fail');
          expect(json.meta).to.not.exist;
          done();
        });
    });
  });
});
