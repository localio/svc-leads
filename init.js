const { spawn } = require('child_process');

const env = process.env.NODE_ENV === 'production' ? 'prod' : 'dev';

console.log(`> run node in ${env}`);

spawn('npm', ['run', env], { stdio: 'inherit' });
