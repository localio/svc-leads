#!/usr/bin/env bash

VERSION=`cat package.json | jq -r '.version'`
NAME=`cat package.json | jq -r '.name'`
CONTAINER_IMAGE=gcr.io/$PROJECT_NAME/${NAME}:v${VERSION}
NAMESPACE=dostolu
CLUSTER=$PROJECT_CLUSTER
PORT=3009

# Prepare output file
IF=cloud-config.yaml
OF=cloud-config-ready.yaml
eval "echo \"$(cat ${IF})\" > ${OF}"
# Build image

docker build -t ${CONTAINER_IMAGE} .
gcloud docker -- push ${CONTAINER_IMAGE}

kubectl apply --record --namespace=${NAMESPACE} -f ${OF}

# Remove config file
rm ${OF}
