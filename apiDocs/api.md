svc-leads API
=============
Simple REST API service.


**Version:** 0.0.1

### /status
---
##### ***GET***
**Summary:** System health

**Description:** Check backend health

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | System is up and running |
| 500 | Troubles on the backend side | [Error](#error) |

### /leads
---
##### ***GET***
**Summary:** Leads list

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| include | query | List of the properties to populate All includes suppose to be from the same service. E.g. include=street,town  | No | string |
| fields | query | List of the fields which might be eiter hidden either shown. To set the field which needs to be showm use just field name. To hide field use the field name with the minus. To set fields, related to the included, use dot notation E.g. fields=-slug,-geometry,name,-included.name  | No | string |
| page | query | In case of -1, which is default case for that,  will show all results as a single page  | No | integer |
| page[offset] | query | Pagination offset, number of items to skip | No | integer |
| page[limit] | query | Pagination limit, number of items per page. Default is 10 | No | integer |
| sort | query | Field name to sort,  ASC - if posititive, DESC - if prefixed with the minus sign  | No | string |
| filters | query | Filters to apply. Could be single and multiple. Multiple filters are comma separated, in opeartor will be used. E.g. fields[slug]=kruk,fields[_id]=1,2,3,4  | No | string |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Leads list | [Leads](#leads) |
| 500 | Troubles on the backend side | [Error](#error) |

##### ***POST***
**Summary:** Create new lead

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | Leads object to create | Yes | [Lead](#lead) |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 201 | Lead has been created | [LeadId](#leadid) |
| 405 | Invalid input | [Error](#error) |
| 500 | Troubles on the backend side | [Error](#error) |

### /leads/{leadId}
---
##### ***GET***
**Summary:** Get lead by ID

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| leadId | path | Lead primary key | Yes | string |
| include | query | List of the properties to populate All includes suppose to be from the same service. E.g. include=street,town  | No | string |
| fields | query | List of the fields which might be eiter hidden either shown. To set the field which needs to be showm use just field name. To hide field use the field name with the minus. To set fields, related to the included, use dot notation E.g. fields=-slug,-geometry,name,-included.name  | No | string |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Lead by ID | [Lead](#lead) |
| 404 | Item not found | [Error](#error) |
| 500 | Troubles on the backend side | [Error](#error) |

##### ***PUT***
**Summary:** Update existing item

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| leadId | path | Lead primary key | Yes | string |
| body | body | Template object to update | Yes | [Lead](#lead) |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 201 | Order has been updates | [LeadId](#leadid) |
| 404 | Item not found | [Error](#error) |
| 405 | Invalid input | [Error](#error) |
| 500 | Troubles on the backend side | [Error](#error) |

##### ***DELETE***
**Summary:** Remove item

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| leadId | path | Lead primary key | Yes | string |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 201 | Item has been removed |
| 404 | Item not found | [Error](#error) |
| 405 | Invalid input | [Error](#error) |
| 500 | Troubles on the backend side | [Error](#error) |

### Models
---

### Error  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| status | string | Status. Error | No |
| message | string | Short problem description | No |
| code | integer | Status code | No |

### Email  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| email | string |  | No |

### Phone  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| phone | string |  | No |

### Comment  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| comment | string | Comment | No |
| date | string | Comment date | No |

### Contact  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| position | string | Contact title (Manager, owner etc) | No |
| phones | [ [Phone](#phone) ] |  | No |
| emails | [ [Email](#email) ] |  | No |
| name | string | Contact first and last names | No |
| comment | string | Short note about contact | No |

### Leads  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| status | string | success | No |
| data | [ [Lead](#lead) ] |  | No |
| meta | [Meta](#meta) |  | No |

### Lead  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| createdAt | string | Creation date | No |
| updatedAt | string | Update date | No |
| type | string | Lead type | No |
| comments | [ [Comment](#comment) ] |  | No |
| phones | [ [Phone](#phone) ] |  | No |
| emails | [ [Email](#email) ] |  | No |
| contacts | [ [Contact](#contact) ] |  | No |
| name | string | Place name | No |
| cuisines | [ string ] |  | No |
| address | string | Address ID | No |
| source | string | Lead source, e.g. Website, Google etc | No |
| status | string | Lead status | No |

### Meta  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| total | integer |  | No |
| offset | integer |  | No |
| limit | integer |  | No |

### LeadId  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| _id | string |  | No |