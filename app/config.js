// Hierarchical node.js configuration with command-line arguments, environment
// variables, and files.
const nconf = require('nconf');
const path = require('path');

/**
 * Check setting existance and throw error if not provided
 * @param {Array} setting Setting name to check
 */
const checkConfig = settings => {
  settings.forEach(setting => {
    if (!nconf.get(setting)) {
      throw new Error(`You must set ${setting} as an environment variable or in config.json!`);
    }
  });
};

nconf
  // 1. Command-line arguments
  .argv()
  // 2. Environment variables
  .env([
    'DB_REDIS_HOST',
    'DB_REDIS_PORT',
    'DB_MONGODB_HOST',
    'DB_MONGODB_USER',
    'DB_MONGODB_PASS',
    'DB_MONGODB_PORT',
    'DB_MONGO_BASE',
    'PORT',
    'SVC_TEMPLATE',
    'TOPIC_ORDERS',
    'TOPIC_CHANNEL',
    'INTL_AVAILABLE',
    'INTL_DEFAULT'
  ])
  // 3. Config file
  .file({
    file: path.join(__dirname, '../config.json')
  })
  // 4. Defaults
  .defaults({
    DB_REDIS_HOST: 'redis',
    DB_REDIS_PORT: '6379',
    DB_MONGODB_HOST: 'mongodb',
    DB_MONGODB_USER: 'user',
    DB_MONGODB_PASS: 'pass',
    DB_MONGODB_PORT: 27017,
    DB_MONGO_BASE: 'base',
    isProduction: process.env.NODE_ENV === 'production',
    isTesting: process.env.NODE_ENV === 'testing',
    PORT: 3009,
    // SVC_ORDERS: 'http://svc-orders:3007',
    // TOPIC_ORDERS: 'order',
    // TOPIC_ORDER_STATUS: 'orders-status',
    // SVC_TEMPLATE: 'http://svc-template:9999',
    // TOPIC_CHANNEL: 'channel',
    // INTL_AVAILABLE: 'uk,en,ru,pl,de',
    // INTL_DEFAULT: 'uk'
  });

// Check required settings
checkConfig([
  'DB_MONGODB_HOST',
  'DB_MONGODB_USER',
  'DB_MONGODB_PASS',
  'DB_MONGODB_PORT',
  'DB_MONGO_BASE',
  'PORT',
  'DB_REDIS_HOST',
  'DB_REDIS_PORT'
]);

/**
 * @typedef {Object}
 *
 * @property {String} DB_MONGODB_HOST
 * @property {String} DB_MONGODB_USER
 * @property {String} DB_MONGODB_PASS
 * @property {String} DB_MONGODB_PORT
 * @property {String} DB_MONGO_BASE
 *
 * @property {String} DB_REDIS_HOST
 * @property {String} DB_REDIS_PORT
 *
 * @property {Boolean} isProduction
 * @property {Number} PORT
 *
 * @property {String} SVC_TEMPLATE
 *
 * @property {String} INTL_AVAILABLE
 * @property {String} INTL_DEFAULT
 */
module.exports = nconf.get();
