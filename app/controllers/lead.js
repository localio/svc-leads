const LeadModel = require('../models/Lead');
const BaseController = require('@dostolu/baseController');

const { schema } = LeadModel;
const schemaFields = Object.keys(schema.obj);

/**
 * Allowed filters
 * @type {string[]}
 */
const filters = schemaFields.concat(['_id']);
/**
 * Allowed fields
 * @type {string[]}
 */
const fields = schemaFields.concat(['_id']);
/**
 * Allowed includes
 * @type {string[]}
 */
const include = [];
/**
 * Allowed sort
 * @type {string[]}
 */
const sort = schemaFields.concat(['_id']);

class MessageController extends BaseController {
  constructor(query) {
    super(query, LeadModel, {
      filters,
      fields,
      include,
      sort
    });
  }
}

module.exports = MessageController;
