const mongoose = require('mongoose');
const { leads: { types: leadTypes } } = require('@dostolu/constants');
const { leads: { status: leadStatus } } = require('@dostolu/constants');
const { lib: { isConstantInRange } } = require('@dostolu/constants');

// Lead schema
const Lead = new mongoose.Schema({
  /**
   * Creation date
   */
  createdAt: {
    type: Date
  },
  /**
   * Updated date
   */
  updatedAt: {
    type: Date
  },
  /**
   * Lead type
   */
  type: {
    type: Number,
    required: true,
    validate: {
      validator: (v) => isConstantInRange(v, leadTypes),
      message: '{VALUE} is not a valid lead type'
    }
  },
  comments: [{
    /**
     * Comment intself
     */
    comment: {
      type: String,
      required: true
    },
    /**
     * Comment date
     */
    date: Date
  }],
  /**
   * Phone numbers
   */
  phones: [{
    phone: String
  }],
  /**
   * Emails
   */
  emails: [{
    email: String
  }],
  /**
   * Lead contacts
   */
  contacts: [{
    position: String,
    phones: [{ phone: String }],
    emails: [{ email: String }],
    name: {
      type: String,
      required: true
    },
    comment: String
  }],
  /**
   * Place name
   */
  name: {
    type: String,
    required: true
  },
  /**
   * Cuisines
   */
  cuisines: [{
    cuisine: String
  }],
  /**
   * Address reference
   */
  address: {
    type: mongoose.Schema.Types.ObjectId
  },
  /**
   * Lead source
   */
  source: String,
  /**
   * Lead status
  */
  status: {
    type: Number,
    required: true,
    default: leadStatus.LEAD_STATUS_NEW,
    validate: {
      validator: (v) => isConstantInRange(v, leadStatus),
      message: '{VALUE} is not a valid lead status'
    }
  },
});

// Set creation date
Lead.pre('save', function (next) { // eslint-disable-line func-names
  if (!this.createdAt) {
    const now = Date.now();
    this.createdAt = now;
    this.updatedAt = now;
  } else {
    this.updatedAt = Date.now();
  }
  next();
});

// Make field to be updated on every update call
Lead.pre('update', function (next) {
  this.update({ }, { $set: { updatedAt: Date.now() } });
  next();
});

const LeadModel = mongoose.model('Lead', Lead);

module.exports = LeadModel;
