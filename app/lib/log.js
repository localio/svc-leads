const winston = require('winston');
const config = require('../config');
const colors = require('colors/safe');

winston.cli();

let level = 'debug';
if (config.isTesting) {
  level = 'warning';
}

module.exports = (module) => {
  // Include filename
  const path = colors.grey(`${module.filename.split('/').slice(-2).join('/')}`);
  return new winston.Logger({
    transports: [
      // Use console
      new winston.transports.Console({
        colorize: true,
        level,
        label: path
      })
    ]
  });
};
