const config = require('../config');
const redis = require('redis');

const options = {
  host: config.DB_REDIS_HOST,
  port: config.DB_REDIS_PORT
};

// Subscibe connection
const sub = redis.createClient(options);
// Publish connection
const pub = redis.createClient(options);
// Read/write connection
const client = redis.createClient(options);

module.exports.pub = pub;
module.exports.sub = sub;
module.exports.client = client;
