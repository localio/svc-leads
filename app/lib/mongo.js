const mongoose = require('mongoose');
const config = require('../config');
const log = require('./log.js')(module);

const DEFAULT_RECONNECT_TIME = 15000;

// Set up default Promise library to be used with mongoose
mongoose.Promise = global.Promise;

const url = `mongodb://${config.DB_MONGODB_HOST}:`
  + `${config.DB_MONGODB_PORT}/${config.DB_MONGODB_BASE}`;
mongoose.connect(url);

const db = mongoose.connection;

db.on('error', (err) => {
  log.error(
    'connection error',
    err.message,
    `trying to reconnect in ${DEFAULT_RECONNECT_TIME / 1000} seconds`
  );
  setTimeout(() => {
    log.info('trying to reconnect....');
    mongoose.connect(url);
  }, DEFAULT_RECONNECT_TIME);
});

db.once('open', () => {
  log.info('establish connection to mongodb');
});

// Enable debug in dev mode only
if (!config.isProduction && !config.isTesting) {
  mongoose.set('debug', true);
}

module.exports = db;
