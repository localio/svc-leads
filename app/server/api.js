const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const morgan = require('morgan');
const japi = require('@dostolu/japi');
const config = require('../config');
const logger = require('../lib/log')(module);
const status = require('./routes/status');
const leads = require('./routes/leads');
const pack = require('../../package.json');

// Connect to mongo database
require('../lib/mongo');

const app = express();

// Set up port
app.set('port', config.PORT);

// Set up middleware

// Set up body parser in order to get post values
app.use(bodyParser.json());

// Use japi
app.use(japi.middleware);

// Allow to override PUT and DELETE methods using custom header
app.use(methodOverride('X-HTTP-Method-Override'));

// Access logs
if (!config.isTesting) {
  app.use(morgan('combined'));
}

// Set up default content type
app.get('*', (req, res, next) => {
  res.header('Content-Type', 'application/json; charset=utf-8');
  // res.header('Access-Control-Allow-Origin', '*');
  // res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH');
  // res.header('Access-Control-Allow-Headers',
  //   'x-forwarded-proto,Accept,DNT,X-CustomHeader,Keep-Alive,User-Agent,' +
  //   'X-Requested-With,If-Modified-Since,Cache-Control,Content-Type');
  next();
});

// Routers

// /status
app.use('/status', status);

// /leads
app.use('/leads', leads);

// Not found error handling
/* eslint-disable no-unused-vars */
app.use((req, res) => {
  logger.warn(`Not found URL: ${req.url}`);
  res.status(404).send(JSON.stringify({
    status: 'error',
    message: 'Not found'
  }));
});

// 500 error handling
app.use((err, req, res, next) => {
  logger.error(`Internal server error: ${err}`);
  res.status(500).send(JSON.stringify({
    status: 'error',
    message: 'Internal server error'
  }));
});
/* eslint-enable no-unused-vars */

// Express error logging
app.on('error', (err) => {
  logger.error(`Express: ${err}`);
});

// Start web server using defined port
app.listen(app.get('port'), () => {
  logger.info(`${pack.name} is running on port`, app.get('port'));
});

module.exports = app;
