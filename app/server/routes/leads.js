const express = require('express');

const router = express.Router();
const LeadController = require('../../controllers/lead');
const validationTransformer = require('@dostolu/validationTransformer');
const { TERM_WRONG_FORMAT, NOT_FOUND_ERROR_NAME } = require('@dostolu/baseController');
const log = require('../../lib/log')(module);

/**
 * Get list
 */
router.get('/', (req, res) => {
  const parameters = { ...req.query, ...req.body };
  const controller = new LeadController(parameters);
  // Get all messages and apply filters if provided,
  // don't share geometry by default
  controller.getList()
    .then((result) => {
      const { data, meta } = result;
      res.status(200).japi.success(data, meta);
    })
    .catch(err => res.status(404).japi.fail(validationTransformer(err)));
});

/**
 * Get by ID
 */
router.get('/:id', (req, res) => {
  const parameters = { ...req.query, ...req.body };
  const controller = new LeadController(parameters);
  controller.getById(req.params.id)
    .then(leads => res.status(200).japi.success(leads))
    .catch((err) => {
      switch (err.name) {
        case TERM_WRONG_FORMAT:
          log.warn(err);
          res.status(405).japi.fail(validationTransformer(err));
          break;
        case NOT_FOUND_ERROR_NAME:
          log.warn(err);
          res.status(404).japi.fail(validationTransformer(err));
          break;
        default:
          log.warn(err);
          res.status(500).japi.fail(validationTransformer(err));
          break;
      }
    });
});

/**
 * Create record
 */
router.post('/', (req, res) => {
  const controller = new LeadController(req.query);
  controller.create(req.body)
    .then(val => res.status(201).japi.success(val))
    .catch((err) => {
      switch (err.name) {
        case TERM_WRONG_FORMAT:
          log.warn(err);
          res.status(405).japi.fail(validationTransformer(err));
          break;
        case NOT_FOUND_ERROR_NAME:
          log.warn(err);
          res.status(404).japi.fail(validationTransformer(err));
          break;
        default:
          log.warn(err);
          res.status(500).japi.fail(validationTransformer(err));
          break;
      }
    });
});

/**
 * Update record
 */
router.put('/:id', (req, res) => {
  const controller = new LeadController(req.query);
  controller.update(req.body, req.params.id)
    .then(val => res.status(201).japi.success({ _id: val }))
    .catch((err) => {
      switch (err.name) {
        case TERM_WRONG_FORMAT:
          res.status(400).japi.fail(validationTransformer(err));
          break;
        case NOT_FOUND_ERROR_NAME:
          res.status(404).japi.fail(validationTransformer(err));
          break;
        default:
          log.warn(err);
          res.status(500).japi.fail(validationTransformer(err));
          break;
      }
    });
});

/**
 * Delete record
 */
router.delete('/:id', (req, res) => {
  const controller = new LeadController(req.query);
  controller.remove(req.params.id)
    .then(() => res.status(204).japi.success({ }))
    .catch((err) => {
      switch (err.name) {
        case TERM_WRONG_FORMAT:
          res.status(405).japi.fail(validationTransformer(err));
          break;
        case NOT_FOUND_ERROR_NAME:
          res.status(404).japi.fail(validationTransformer(err));
          break;
        default:
          log.warn(err);
          res.status(500).japi.fail(validationTransformer(err));
          break;
      }
    });
});

module.exports = router;
