const config = require('../config');
const { sub } = require('../lib/redis');
const log = require('../lib/log')(module);

// subscribe to restaurant channel
sub.subscribe(config.TOPIC_CHANNEL);

sub.on('message', (channel, message) => {
  // Track restaurant channel
  log.debug(`sub channel ${channel}: ${message}`);
  switch (channel) {
    case config.TOPIC_CHANNEL:
      log.info(message);
      break;
    default:
      log.warn(`Wrong channel: ${channel}`);
  }
});
